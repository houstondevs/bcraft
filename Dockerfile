FROM python:3.9.1

ADD . /bcraft

WORKDIR /bcraft

RUN pip install -r requirements/requirements.txt