from django_filters import rest_framework as filters, DateFromToRangeFilter


class StatisticFilter(filters.FilterSet):
    date = DateFromToRangeFilter()