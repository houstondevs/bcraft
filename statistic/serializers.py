from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Statistic


class CreateStatisticSerializer(serializers.ModelSerializer):
    cpc = serializers.ReadOnlyField()
    cpm = serializers.ReadOnlyField()

    class Meta:
        model = Statistic
        fields = '__all__'
        extra_kwargs = {
            'views': {'required': False, 'min_value': 0},
            'cost': {'required': False, 'min_value': 0},
            'clicks': {'required': False, 'min_value': 0},
        }


class ReadStatisticSerializer(serializers.ModelSerializer):
    views = serializers.ReadOnlyField(source='total_views')
    clicks = serializers.ReadOnlyField(source='total_clicks')
    cost = serializers.ReadOnlyField(source='total_cost')
    cpc = serializers.ReadOnlyField()
    cpm = serializers.ReadOnlyField()

    class Meta:
        model = Statistic
        fields = '__all__'


class DatePeriodSerializer(serializers.Serializer):
    start = serializers.DateField(required=True)
    end = serializers.DateField(required=True)

    def validate(self, attrs):
        start = attrs.get('start', None)
        end = attrs.get('end', None)

        if start and end:
            if end < start:
                raise ValidationError({'end': 'Дата конца не может быть раньше даты начала.'})
        return attrs