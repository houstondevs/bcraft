from django.utils.timezone import now, timedelta
from rest_framework import status
from rest_framework.reverse import reverse

from rest_framework.test import APITestCase

from model_bakery import baker

from statistic.models import Statistic


class StatisticApiTest(APITestCase):
    def setUp(self) -> None:
        self.url = reverse('statistic-list')
        self.base_clicks = 5
        self.base_views = 10
        self.base_cost = 50.00

    def create_statistic(self):
        dates = [(now() - timedelta(days=i)).date() for i in range(30)]
        for date in dates:
            baker.make('statistic.Statistic', date=date, views=self.base_views, clicks=self.base_clicks,
                       cost=self.base_cost)

    def test_clear_statistic(self):
        self.create_statistic()
        self.assertEqual(Statistic.objects.count(), 30)
        url = reverse('statistic-clear-statistic')
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Statistic.objects.count(), 0)

    def test_list(self):
        self.create_statistic()
        self.create_statistic()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {'start': now().date().strftime('%Y-%m-%d')}
        response = self.client.get(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data['end'] = (now() - timedelta(days=1)).date().strftime('%Y-%m-%d')
        response = self.client.get(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data['end'] = now().date().strftime('%Y-%m-%d')
        response = self.client.get(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['date'], now().date().strftime('%Y-%m-%d'))
        self.assertEqual(response.data[0]['cpc'], self.base_cost / self.base_clicks)
        self.assertEqual(response.data[0]['cpm'], self.base_cost / self.base_views * 1000)

        data['start'] = (now() - timedelta(30)).date().strftime('%Y-%m-%d')
        response = self.client.get(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 30)
        for item in response.data:
            self.assertEqual(item['cpc'], self.base_cost / self.base_clicks)
            self.assertEqual(item['cpm'], self.base_cost / self.base_views * 1000)

        # Деление на 0
        Statistic.objects.filter(date=now().date()).update(clicks=0, views=0)
        response = self.client.get(self.url, {'start': now().date(), 'end': now().date()})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['cpc'], 0)
        self.assertEqual(response.data[0]['cpm'], 0)

        # Неверный формат даты
        data['end'] = now().date().strftime('%Y.%m.%d')
        response = self.client.get(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('end', response.data)

    def test_create(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('date', response.data)

        data = {'date': now().strftime('%Y-%m-%d')}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        created_object = Statistic.objects.all().first()
        self.assertEqual(created_object.cost, 0.00)
        self.assertEqual(created_object.views, 0)
        self.assertEqual(created_object.clicks, 0)
        created_object.delete()

        fields = ['cost', 'views', 'clicks']
        data = {field: -10 for field in fields}
        data['date'] = now().date()

        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        for field in fields:
            self.assertIn(field, response.data)

        for field in fields:
            data[field] = 10
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        created_object = Statistic.objects.all().first()
        self.assertEqual(created_object.cost, 10.00)
        self.assertEqual(created_object.views, 10)
        self.assertEqual(created_object.clicks, 10)