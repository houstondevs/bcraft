from django.db.models import F, DecimalField, Case, When, Sum
from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.decorators import action

from bcraft.utils import Round
from .models import Statistic
from .serializers import CreateStatisticSerializer, ReadStatisticSerializer, DatePeriodSerializer


class StatisticViewSet(mixins.ListModelMixin,
                       mixins.CreateModelMixin,
                       viewsets.GenericViewSet):
    queryset = Statistic.objects.all()
    serializer_class = ReadStatisticSerializer
    order_dict = {
        'views': 'total_views',
        '-views': '-total_views',
        'clicks': 'total_clicks',
        '-clicks': '-total_clicks',
        'cost': 'total_cost',
        '-cost': '-total_cost',
    }

    def get_serializer_class(self):
        if self.action == 'list':
            return DatePeriodSerializer
        elif self.action == 'create':
            return CreateStatisticSerializer
        return self.serializer_class

    def get_queryset(self):
        qs = super().get_queryset()
        if self.action == 'list':
            ordering = [self.order_dict[order] if order in self.order_dict else order
                        for order in self.request.query_params.getlist('ordering', ['date'])]
            return qs.filter(
                date__range=[self.request.query_params.get('start'), self.request.query_params.get('end')]
            ).values('date').annotate(
                total_views=Sum('views'),
                total_clicks=Sum('clicks'), total_cost=Sum('cost'),
                cpc=Case(When(total_clicks=0, then=0), default=Round(F('total_cost') / F('total_clicks')),
                         output_field=DecimalField()),
                cpm=Case(When(total_views=0, then=0), default=Round(F('total_cost') / F('total_views') * 1000),
                         output_field=DecimalField()),
            ).order_by(*ordering).values('date', 'total_views', 'total_clicks', 'total_cost', 'cpc', 'cpm')
        return qs

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.query_params)
        if serializer.is_valid():
            data = self.get_queryset()
            return Response(self.serializer_class(instance=data, many=True).data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['DELETE'])
    def clear_statistic(self, request):
        Statistic.objects.all().delete()
        return Response('Success', status=status.HTTP_204_NO_CONTENT)
