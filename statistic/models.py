from django.db import models


class Statistic(models.Model):
    class Meta:
        verbose_name = 'Статистика'
        verbose_name_plural = 'Статистики'

    date = models.DateField(verbose_name='Дата события')
    views = models.PositiveBigIntegerField('Количество просмотров', default=0)
    clicks = models.PositiveBigIntegerField('Количество кликов', default=0)
    cost = models.DecimalField(verbose_name='Стоимость кликов', max_digits=9, decimal_places=2, default=0.00)

    def __str__(self):
        return f"Событие за {self.date}"
